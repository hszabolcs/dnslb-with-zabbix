# DNSBL check (23) #

Domain Name System-based Blackhole List (DNSBL) check

### Requirements ###

CentOS/RHEL
# yum install bind-utils
Debian/Ubuntu
# apt-get install dnsutils

### How do I get set up? ###


Copy "check_dnsbl.sh" to your Zabbix Servers "/usr/local/share/zabbix/externalscripts" *
* Check your server configuration file for the correct folder, look for the tag "ExternalScripts"

Make the script executable: chmod +x /usr/local/share/zabbix/externalscripts/check_dnsbl.sh

Create the following value map (Administration -> General -> Value mapping: Create value map)

Name: IP Blacklist
0 -> Not listed
1 -> Listed

Import the template and assign it to your host(s).